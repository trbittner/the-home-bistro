** Pasta **

* [Tomato Basil Chicken Pasta](http://damndelicious.net/2016/02/02/tomato-basil-chicken-fettuccine/?m)
* [Three Cheese and Spinach Lasagna]()
* [Ricotta Dumplings and Spinach Brown Butter Sauce]()
* [Fettucine Alfredo](http://allrecipes.com/recipe/22831/alfredo-sauce/)
* [Sweet Potato Fettucine](http://www.epicurious.com/recipes/food/views/Sweet-Potato-Ravioli-with-Sage-Butter-Sauce-104709)
* [Spaghetti with Shrimp and Lemon Cream]()
* [Ricotta and Spinach Ravioli]()

** Pizza **

* [Spinach, Sun-dried Tomato, and Mozzarella Pizza]()
* [Buffalo Style Chicken Pizza](http://allrecipes.com/recipe/buffalo-style-chicken-pizza/)
* [Pizza Margherita](https://www.epicurious.com/recipes/food/views/pizza-margherita-351165)
* [Pepperoni Pizza](http://allrecipes.com/recipe/44975/easy-pizza-sauce-iii/)
* [Angel Hair with Basil Cream](http://allrecipes.com/recipe/quick-angel-hair-with-basil-cream-for-one/)

** Sandwiches **

* [Cheeseburgers](http://www.thekitchn.com/how-to-make-burgers-on-the-stovetop-cooking-lessons-from-the-kitchn-217722)
* [Black Bean Burgers](http://allrecipes.com/recipe/homemade-black-bean-veggie-burgers/)
* [Chicken Sandwich](https://hilahcooking.com/chick-fil-a-copycat/)
* [Quick Cubano]()
* [Crab Po' Boy](http://www.epicurious.com/recipes/food/views/Crab-Burger-Po-Boys-13027)
* [Eggplant, Roasted Pepper, Pesto, and Mozzarella Sandwich]()
* [Muffaletta Sandwich]()
* [Chicken Cutlets with Basil and Mozzarella (or Arugula and Cheddar)]()
* [New England Crab Rolls](https://www.epicurious.com/recipes/food/views/new-england-crab-rolls-108444)

** Meat **

* [Gorgonzola Pork Tenderloin](http://www.epicurious.com/recipes/food/views/Pork-with-Gorgonzola-Sauce-104590)
* [Marinated Flank Steak](https://www.epicurious.com/recipes/food/views/Grilled-Flank-Steak-with-Rosemary-731)

** Fish **
* [Parmesan Broiled Tilapia](https://www.spendwithpennies.com/parmesan-broiled-tilapia/)
* [Salmon with Sour Cream, Mustard and Dill](https://www.epicurious.com/recipes/food/views/baked-salmon-with-mustard-dill-sauce-5459)
* [Salmon with Cream, Dill, and Endive](http://www.epicurious.com/recipes/food/views/Salmon-with-Endive-Dill-and-Cream-232799)
* [Pan Seared Tilapia with Chili Lime Butter](https://www.epicurious.com/recipes/food/views/Pan-Seared-Tilapia-with-Chile-Lime-Butter-108343)

** Poultry **

* [Fried Chicken](http://www.epicurious.com/recipes/food/views/Rosemary-Brined-Buttermilk-Fried-Chicken-368790)
* [Roast Chicken](http://www.epicurious.com/recipes/food/views/My-Favorite-Simple-Roast-Chicken-231348)
* [Oven Roasted Chicken Fingers](https://www.bettycrocker.com/recipes/oven-fried-chicken-tenders/46a69e4d-4c31-485b-9d43-67d40b353f2e)
* [Chicked Stuffed with Feta and Kale](http://inthekitchenwithzoe.com/blog/2014/04/03/feta-and-kale-stuffed-chicken-breast/)
* [Wilted Spinach and Warm Feta Dressing Salad](https://www.epicurious.com/recipes/food/views/wilted-spinach-salad-with-warm-feta-dressing-240677)
* [Garlic Parmesan (or Baked Buffalo) Wings](http://www.geniuskitchen.com/recipe/garlic-parmesan-chicken-wings-287041)

** Vegetarian **

* [Eggplant Parmigiana]()
* [Vegetarian Pasta A La Grecque](http://www.myrecipefriends.com/recipe/78691.html)
* [Black Bean Soup](http://www.epicurious.com/recipes/food/views/Quick-Black-Bean-Soup-101661)
* [Cheddar and Beer Soup](http://allrecipes.com/recipe/wisconsin-natives-beer-cheese-soup/)
