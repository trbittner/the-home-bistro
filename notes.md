** Notes **

* `sudo apt-get update`
* `sudo apt-get install python-pip`
* `sudo pip install mistune`
* [Mistune](https://github.com/lepture/mistune) is a markdown parser for python.
* Rather than construct meals by trying to interlace ingredients, it might be worthwhile to suggest meals and then add further instructions for timing and composition.
* We'll probably want to use [Jinja2](http://jinja.pocoo.org/) for templating when building from markdown.
* And, we'll probably use [pytest](https://docs.pytest.org/en/latest/) for testing.
* Python modules and packages are generally a pain.  [Here's](https://docs.python.org/2/tutorial/modules.html) the only reference that comes close to explaining where things should reside.
* One thing for ease of use - make sure white spaces are ignored for markdown and that we react appropriately when certain sections are missing.
* Also make sure that markup in the md file is escaped, whereas that from bs4 is put in verbatim, even as a mix.
* [This](https://stackoverflow.com/questions/38290588/extracting-html-between-tags) may provide a way select content between two tags.  Keep in mind to look for EOF or mal-formed input as well.
* [This is a link on enumerations for Python](https://stackoverflow.com/questions/702834/whats-the-common-practice-for-enums-in-python).
