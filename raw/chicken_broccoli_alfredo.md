## Chicken Broccoli Alfredo

_Time_: 35 minutes
_Yield_: 2 servings

### Ingredients
* 1/2 lb chicken breasts
* 1/2 cup cream
* 2 tbsp butter
* 1 tbsp olive oil
* Salt and pepper
* 8 oz frozen broccoli florets
* 3/4 cup parmesan or asiago
* 1 clove garlic
* 4 oz. Linguine, spaghetti or other similar pasta

## Instructions
1. Preheat oven to 450 degrees.
2. Mix olive oil, salt and pepper in a small bowl.
3. Pound or filet chicken breasts to be even thickness - approximately 1/2 inch.
4. Mince garlic.
5. Cover chicken breasts in olive oil mixture.
6. Fill a medium sauce pan half full of water and salt liberally.
7. Place sauce pan, covered, over high heat.
8. When the oven is pre-heated and the water is boiling, switch the oven to broil.
9. Heat butter over medium heat in a small sauce pan.
10. Place chicken in broiler with chicken approximately 6 inches from heat source.
11. Broil chicken 5 minutes per side.  Cook pasta according to directions - cooking to al dente, approximately 8-10 minutes.  Cook garlic in small sauce pan for 1-2 minutes.
12. Turn heat for small sauce pan to low and add cream and cheese.  Heat until melted, 1-2 minutes.  Mix thoroughly. Keep warm.
13. Place broccoli in a microwave safe container with 2 tbsp. water. 
14. Microwave broccoli until bright green, 3-4 minutes.
15. Drain pasta.
16. Slice chicken crosswise and add to pasta. 
18. Add sauce and broccoli to chicken and pasta mixture. 
19. Combine and serve.
