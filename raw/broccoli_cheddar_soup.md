## Broccoli Cheddar Soup

_Time_: 30 minutes
_Yield_: 4 servings (1 cup)

### Ingredients
* 8 oz. frozen broccoli florets
* 1/2 medium potato
* 2 cups sharp cheddar cheese
* 14.5 oz. low sodium chicken broth
* 2 celery hearts
* 1 cup water
* 1/4 cup heavy cream
* 1 tbsp. butter

### Instructions
1. Chop the potato and celery into approximately 1 in. dice.
2. Heat butter over medium-high heat in medium sauce pan until melted.
3. Add potato and celery and cook for approximately 4 minutes.
4. Add all remaining ingredients, except for cream and cheddar.
5. Adjust heat to simmer after mixture begins boiling.
6. Continue cooking for about 20 minutes, stirring occasionally, until potato breaks apart easily.
7. Add cream and cheddar, stir for approximately 2 minutes until heated through and melted.
8. Puree mixture in blender for approximately 30 seconds.
