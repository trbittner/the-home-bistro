import mistune
from bs4 import BeautifulSoup

markdown = mistune.Markdown()

class Status:
  Success,ErrorMissingTitle,ErrorMissingHeader,ErrorMissingList = range(4)

html_header = '''
<html>

<head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <title>Recipes</title>
</head>

<body>
<container>
<div class="col-md-6">
'''

html_footer = '''
</div>
</container>
</body>
</html>
'''

def read_data_from_markdown_file(filename):
  with open(filename) as f:
    raw_output = f.readlines()
    recipes = [markdown(r) for r in raw_output]
  return recipes

def convert_markdown_string_to_html(markdown_string):
  output = markdown(markdown_string).replace('\n','')
  return output

def format_title(raw_html):
  soup = BeautifulSoup(raw_html,'html.parser')
  tag = soup.h1
  if tag == None:
    return Status.ErrorMissingTitle
  tag['id'] = 'recipe_title'
  return str(tag)

def format_statistics(raw_html):
  soup = BeautifulSoup(raw_html,'html.parser')
  header = soup.find('h2',string="Statistics")
  if not header:
    return
  
  target_soup = BeautifulSoup('<div id="statistics"></div>','html.parser')
  
  p_tag = header.find_next_sibling('p')
  
  if not p_tag:
    return
  
  yield_tag = p_tag.find('em',string = 'Yield:')
  time_tag = p_tag.find('em',string = 'Time:')
  calorie_tag = p_tag.find('em', string = "Calories:")
  stats_div = target_soup.find(id = 'statistics')
  
  formatted_yield = _format_stats_section_(yield_tag,'yield',target_soup)
  if formatted_yield:
    stats_div.append(formatted_yield)
    
  formatted_time = _format_stats_section_(time_tag,'time',target_soup)
  if formatted_time:
    stats_div.append(formatted_time)
    
  formatted_calories = _format_stats_section_(calorie_tag,'calories',target_soup)
  if formatted_calories:
    stats_div.append(formatted_calories)
    
  return target_soup.prettify()

def _format_stats_section_(tag,id_name,soup):
  new_tag = None
  if tag:
    new_tag = soup.new_tag('div',id=id_name)
    new_tag.string = id_name.capitalize() + ': ' + tag.next_sibling.strip()
  return new_tag
    
def format_description(raw_html):
  soup = BeautifulSoup(raw_html,'html.parser')
  header = soup.find('h2',string='Description')
  
  if not header:
    return None
  
  header['id'] = 'description_header'
  target_soup = BeautifulSoup(str(header),'html.parser')
  description = header.next_sibling
  if description:
    description['id'] = 'description'
    target_soup.append(description)
    
  return target_soup.prettify()

def format_section(raw_html,header_name):
  soup = BeautifulSoup(raw_html,'html.parser')
  header = soup.find('h2',string=header_name)
  if header == None:
    return Status.ErrorMissingHeader
  id_name = header_name.lower()
  header['id'] = id_name + '_header'
  target_soup = BeautifulSoup(str(header),'html.parser')
  
  ul_tag = header.find_next_sibling('ul')
  if ul_tag == None:
    return Status.ErrorMissingList
  ul_tag['id'] = id_name
  target_soup.append(ul_tag)
  
  return target_soup.prettify()
  
def convert_recipe_markdown_to_html(recipe_markdown_list,output_filename):
  with open(output_filename,'w') as f:
    f.write(html_header)
    f.write('\n')
    f.writelines(recipe_markdown_list)
    f.write('\n')
    f.write(html_footer)

if __name__ == '__main__':
  recipe_markdown_list = read_recipes_from_markdown_file('recipe_list.md')
  convert_recipe_markdown_to_html(recipe_markdown_list,'recipe.html')
    