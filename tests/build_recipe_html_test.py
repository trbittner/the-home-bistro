import src.build_recipe_html as build_recipe_html

full_markdown = """
# Recipe Title

## Statistics

_Yield:_ Number of servings.
_Time:_ Total time to cook.
_Calories:_ Number of calories for this particular dish.

## Description

This is where the description resides for the recipe.

## Image

[Photo Description](#)

## Ingredients

* Ingredient 1
* Ingredient 2
* Ingredient 3

## Instructions

* Instruction 1
* Instruction 2
* Instruction 3
"""

full_markup = \
'<h1>Recipe Title</h1>' + \
'<h2>Statistics</h2>' + \
'<p><em>Yield:</em> Number of servings.<em>Time:</em> Total time to cook.' + \
'<em>Calories:</em> Number of calories for this particular dish.</p>' + \
'<h2>Description</h2><p>This is where the description resides for the recipe.</p>' + \
'<h2>Image</h2><p><a href="#">Photo Description</a></p>' + \
'<h2>Ingredients</h2>' + \
'<ul><li>Ingredient 1</li><li>Ingredient 2</li><li>Ingredient 3</li></ul>' + \
'<h2>Instructions</h2>' + \
'<ul><li>Instruction 1</li><li>Instruction 2</li><li>Instruction 3</li></ul>'

ingredient_markup = """<h2 id="ingredients_header">
 Ingredients
</h2>
<ul id="ingredients">
 <li>
  Ingredient 1
 </li>
 <li>
  Ingredient 2
 </li>
 <li>
  Ingredient 3
 </li>
</ul>"""

statistics_markup = """<div id="statistics">
 <div id="yield">
  Yield: Number of servings.
 </div>
 <div id="time">
  Time: Total time to cook.
 </div>
 <div id="calories">
  Calories: Number of calories for this particular dish.
 </div>
</div>"""

def test_convert_markdown_string_to_html():
  markdown_output = build_recipe_html.convert_markdown_string_to_html(full_markdown)
  assert markdown_output == full_markup

def test_format_title():
  html_output = build_recipe_html.format_title(full_markup)
  assert html_output == '<h1 id="recipe_title">Recipe Title</h1>'
  
def test_format_missing_title():
  html_output = build_recipe_html.format_title('<html></html>')
  assert html_output == build_recipe_html.Status.ErrorMissingTitle
  
def test_format_missing_statistics_header():
  html_output = build_recipe_html.format_statistics('<html></html>')
  assert html_output == None
  
def test_format_missing_statistics_paragraph():
  html_output = build_recipe_html.format_statistics('<html><h2>Statistics</h2></html>')
  assert html_output == None
  
def test_format_missing_statistics():
  html_output = build_recipe_html.format_statistics('<html><h2>Statistics</h2><p></p></html>')
  assert html_output == '<div id="statistics">\n</div>'
  
def test_format_statistics():
  html_output = build_recipe_html.format_statistics(full_markup)
  assert html_output == statistics_markup
  
def test_format_missing_description_header():
  html_output = build_recipe_html.format_description('<html></html>')
  assert html_output == None
  
def test_format_missing_description():
  html_output = build_recipe_html.format_description('<html><h2>Description</h2></html>')
  assert html_output == '<h2 id="description_header">\n Description\n</h2>'
  
def test_format_description():
  html_output = build_recipe_html.format_description(full_markup)
  assert html_output == '<h2 id="description_header">\n Description\n</h2>\n' + \
  '<p id="description">\n This is where the description resides for the recipe.\n</p>'
  
def test_format_section():
  html_output = build_recipe_html.format_section(full_markup,"Ingredients")
  assert html_output == ingredient_markup
  
def test_format_missing_section_header():
  html_output = build_recipe_html.format_section('<html></html>',"Ingredients")
  assert html_output == build_recipe_html.Status.ErrorMissingHeader
  
def test_format_missing_section_list():
  html_output = build_recipe_html.format_section('<html><h2>Ingredients</h2></html>',"Ingredients")
  assert html_output == build_recipe_html.Status.ErrorMissingList
  