** Description **
A simple method for making fresh ravioli that doesn't require making dough from scratch.

**Yield:** Serves: 2 (approximately 20 ravioli)
**Calories:** Approximately 450 calories
** Total cooking time:** 30 minutes

** Ingredients **

* 10 ounces fresh spinach
* 1 cup whole milk ricotta cheese
* 1/2 teaspoon salt
* 1/4 teaspoon pepper
* 1/4 teaspoon nutmeg
* Wonton or eggroll wrappers, thawed (this will take about 30 minutes in the refrigerator if you haven't done so already)

** Directions **

1. Place 1/2 cup of water in a large pot. Set heat to medium-high.
2. Add spinach and cover. Stirring occasionally for about 5 minutes.
3. While the spinach is cooking, if you're using eggroll wrappers, stack 5 wrappers and cut into rough quarters.
4. Remove spinach from heat, drain, rinse, and compress to remove as much moisture as possible.
5. Place spinach in a small food chopper or food processor and pulse until spinach is chopped.
6. Using the large pot, fill half way with water and salt liberally.  Set heat to high and cover.
7. Mix the chopped spinach, ricotta, nutmeg, salt, and pepper in a small bowl.
8. Fill another small bowl with 1/2 cup water.
9. Sprinkle flour liberally on a large plate and set aside.  You'll place the prepped ravioli here.
10. Working 4 wrappers at a time, place about 1 teaspoon worth of the ricotta mixture in the middle of each wrapper.
11. Dip your index finger in the water and trace two touching edges of the wonton wrapper with your finger.
12. Fold the wrapper diagonally, making sure the edges are completely sealed.
13. Set the ravioli on the plate and repeat.
14. Using a slotted spoon or similar device, place half the ravioli in the salted boiling water.  The ravioli only require 1-2 minutes cooking time and will float when done.
15. Remove the cooked ravioli and repeat with the second batch.
16. Serve.