#! /usr/bin/env python

import sys

import mistune
from bs4 import BeautifulSoup

markdown = mistune.Markdown()

with open(sys.argv[1]) as f:
  raw_output = f.read()
  
raw_html = markdown(raw_output)
soup = BeautifulSoup(raw_html,'html.parser')

with open(sys.argv[2],'w') as f:
  f.write(soup.prettify())
